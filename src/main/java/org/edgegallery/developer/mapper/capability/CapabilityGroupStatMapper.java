package org.edgegallery.developer.mapper.capability;

import java.util.List;

import org.edgegallery.developer.model.capability.CapabilityGroupStat;

public interface CapabilityGroupStatMapper {
	public List<CapabilityGroupStat> selectAll();
}
