/*
 *    Copyright 2021 Huawei Technologies Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.edgegallery.developer.service.capability;

import java.util.List;

import org.edgegallery.developer.model.capability.Capability;
import org.edgegallery.developer.response.FormatRespDto;

import com.spencerwi.either.Either;

public interface CapabilityService {
	public Either<FormatRespDto, Capability> create(Capability capability);

	public Either<FormatRespDto, Capability> deleteById(String id);

	public Either<FormatRespDto, Capability> updateById(Capability capability);

	public List<Capability> findAll();

	public Capability findById(String id);

	public List<Capability> findByGroupId(String groupId);
	
	public List<Capability> findByProjectId(String projectId);

	public Capability findByName(String name);

	public List<Capability> findByNameWithFuzzy(String name);

	public List<Capability> findByNameEnWithFuzzy(String nameEn);
	
	public List<Capability> findByNameOrNameEn(String name, String nameEn);
	
	public boolean updateSelectCountByIds(List<String> ids);
}
